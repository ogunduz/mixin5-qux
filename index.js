
function assert (b) {
  if (!b) throw 'doh!';
}

// kendi halinde class
function Qux () {
}

Qux.prototype.quux = function () {
  return 'quux';
};

// mixin ozelligi olan class
var View = require('mixin5-view');

// Qux'un prototype'i mixliyoruz
View(Qux.prototype);

var qux = new Qux;

// quux metodu zaten var
assert(qux.quux() === 'quux');

// artik foo da var
assert(qux.foo() === 'bar');

// bunu illa prototype'a mixlemek zorunda da degiliz tabiki

var bar = {baz: 1};
View(bar);

assert(bar.foo() === 'bar');
